// Algorítmos para o jogo Smite
const {gods} = require("./arquivo_exercicio_4.js");

// Q1:
for (var j = 0; j < gods.length; j++) {console.log(gods[j]["name"], gods[j]["features"].length);}


// Q2:
for (var j = 0; j < gods.length; j++){
    var m = (gods[j]["roles"])

    if (m.find(elem => elem === "Mid")){
        console.log(gods[j]);
    }
}


// Q3:
gods.sort(function (a, b) {
    if (a.pantheon < b.pantheon) {
        return -1;
    } else {
        return true;
    }
});
console.log(gods);


// Q4:
var lista = [];

for (var j = 0; j < gods.length; j++){
    var n = (gods[j]["name"])
    var c = (gods[j]["class"])

    lista.push(n + " (" + c + ")");
}

for (var j = 0; j < lista.length; j++){
    console.log(lista[j]);
}
