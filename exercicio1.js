// Algoritmo para multiplicar matrizes 2x2

// valores testes
var matriz1 = [ [4,0], [-1,-1] ];
var matriz2 = [ [-1,3], [2,7] ];


function multiplicar(m1, m2){
    a = (m1[0][0] * m2[0][0]) + (m1[0][1] * m2[1][0]); 
    b = (m1[0][0] * m2[0][1]) + (m1[0][1] * m2[1][1]); 
    c = (m1[1][0] * m2[0][0]) + (m1[1][1] * m2[1][0]); 
    d = (m1[1][0] * m2[0][1]) + (m1[1][1] * m2[1][1]);

    var matriz3 = [[a, b], [c, d]];
    console.log(matriz3);
}

(matriz1[0].length == matriz2.length) ? multiplicar(matriz1, matriz2) : console.log("Matriz invalida para operação");

// if (matriz1[0].length == matriz2.length){
//     multiplicar(matriz1, matriz2)
// }else{
//     console.log("Matriz invalida para operação")
// }