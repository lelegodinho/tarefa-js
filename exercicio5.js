// Algoritmo para testar 5 numeros aleatorios

// valores teste
var numeros = [15, 3, 74, 1, 10]

for (num of numeros){
    if ((num % 3 == 0) && (num % 5 == 0)) {
        console.log("fizzbuzz");
    }else if (num % 3 == 0) {
        console.log("fizz");
    }else if (num % 5 == 0){
        console.log("buzz");
    }
}
