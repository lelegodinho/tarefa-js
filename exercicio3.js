//  Algoritmo para média aritimética de notas 

const redutor = (somaValor, valorAtual) => somaValor + valorAtual;

// valor teste
var notas = [7, 5, 8]

var soma = notas.reduce(redutor);
soma >= 18 ? (a = "Aprovado") : (a = "Reprovado");
console.log(a);


// Outra forma sem a const:
// var soma = notas.reduce((soma, i) => soma + i);